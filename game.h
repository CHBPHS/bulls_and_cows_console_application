#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include <stdbool.h>

#undef SHOW
#undef SHOW_HIDDEN_WORD

#define MAXPENALTY 10
#define MAXWORDLENGTH 10


#define FILENAME "dictionary.txt"

void PlayGame();
void RandomWord();
void Introduction();
void DrawBullsAndCows();
void game();

bool AskToPlayAgain();

int Bulls();
int Cows();
int NumberOfLines();

char HiddenWord[1];
char UserInput[MAXWORDLENGTH];

#endif // GAME_H_INCLUDED
