#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include "game.h"

void game(){

    bool game = false;

    do{
        RandomWord();

        Introduction();

        PlayGame();

        game = AskToPlayAgain();

    }while(game);

}

void PlayGame(){

    int Penalty = MAXPENALTY;

    DrawBullsAndCows();

    for(int k = 0; k <= MAXPENALTY; k++){ //asks word until the correct lenght

        printf("Enter you guess!\n");
        printf("You have %d tries!\n\n",Penalty);
        printf("My guess is: ");

        scanf("%s",UserInput);

        printf("\n");


        if(strlen(UserInput) != strlen(HiddenWord)){ //nem megfelelő a szó hosszúság
            printf("Wrong length!\n");
            Penalty--;
            printf("_________________________________\n");

            if(Penalty == 0)
                break;
        }
        else{ //megfelelõ hosszúságú a szó
            if(strcmp(HiddenWord,UserInput) == 0){ //jó szó esetén kilépés
                printf("\n\nCongratulations!\nYou have guessed the Word!\n\n");
                break;
            }
            else{ // rossz szó esetén új szó és bünti de lefut a bull és cow keresés
                printf("Wrong word!\n");
                Penalty--;

                //bulls test
                printf("\nBulls: %d \n", Bulls());

                //cows test
                printf("Cows: %d \n\n", Cows());

                printf("_________________________________\n");
            }
        }
    }

    //penalty exit
    if(Penalty == 0)
        printf("\nI won!\nYou could not guess my word! ;) \n\n");

    return EXIT_SUCCESS;

}

int Bulls(){

    int NumOfBulls = 0;

    /** megkapja a hidden word ot és a user inputot
        hogyha az UserInput első betűje eggyezik a Hidden word első betűjével akkor NumOfBulls++
        és így tovább a szó végéig
        annyi bull van amennyi pontosan eggyező betű van helyiérték szerint
    **/

    char *pUserInput = (char*)malloc((MAXWORDLENGTH)+1);
    char *pHiddenWord = (char*)malloc(MAXWORDLENGTH+1);

    pUserInput = UserInput;
    pHiddenWord = HiddenWord;
    int ptr = 0;

#ifdef SHOW
    printf("\n\n%s, %s\n\n",pUserInput, pHiddenWord); //pointer kapott értéke
#endif // SHOW

    int i = 0;

    while(i < strlen(HiddenWord)){

        if(pHiddenWord[ptr] == pUserInput[ptr])
            NumOfBulls++;

        ptr++;
        i++;
    }

    pUserInput = NULL;
    pHiddenWord = NULL;

    free(pUserInput);
    free(pHiddenWord);

    return NumOfBulls;
}

int Cows(){

    int NumOfCows = 0;
    /**
        A Cows feladata hogy ki írja azt, hogy mennyi olyan betű van a UserInput szóban ami megtalálható a HiddenWord szóban
    **/

    char *pUserInput = (char*)malloc(strlen(UserInput)+1);
    char *pHiddenWord = (char*)malloc(strlen(HiddenWord)+1);

    pUserInput = UserInput;
    pHiddenWord = HiddenWord;

    for(int i = 0; i < strlen(HiddenWord); i++)
        for(int k = 0; k< strlen(HiddenWord); k++)
            if(pHiddenWord[i] == pUserInput[k])
                NumOfCows++;

    NumOfCows = NumOfCows - Bulls();

    pUserInput = NULL;
    pHiddenWord = NULL;

    free(pUserInput);
    free(pHiddenWord);

    return NumOfCows;
}

//a fileból keres magának egy random szót
void RandomWord(){

    srand((unsigned) time(0));

    int NOL = NumberOfLines();
    char WordList[NOL + 1][MAXWORDLENGTH];

    //kiválaszt egy random sort
    long i = rand() % NOL;

#ifdef SHOW_HIDDEN_WORD
    printf("Number of Lines: %d \n",NOL);
    printf("Random sor: %ld\n\n",i);
#endif // SHOW_HIDDEN_WORD

    FILE *fp = fopen(FILENAME, "r"); //file megnyitása

    if(fp == NULL) //nem sikerült megnyitni
        return EXIT_FAILURE;

    for(int j = 0; j < NOL; j++){
        fscanf(fp,"%s",WordList[j]);
    }

    fclose(fp); //file bezárása
    fp = NULL;

    strcpy(HiddenWord, WordList[i]);

}

//megszámolja a sorokat
int NumberOfLines(){

    FILE *fp = fopen(FILENAME,"r");

    int count = 0;
    char c;

    if(fp == NULL)
        return (-1);

    while((c = fgetc(fp)) != EOF){
        if(c == '\n')
            count++;
    }
//    printf("\nNumber of lines: %d \n",++count);

    return count;
}

//game and rule introductions
void Introduction(){

    printf("        This is a Bulls & Cows game!\n\n");
    printf("\t\tRules:\n\n");
    printf("You have to guess the %d letter word\n",strlen(HiddenWord));
    printf("Bull means that you have found a letter that has the same location in my word!\n");
    printf("Cow means that you have found a letter that has in my word!\n");
    printf("If you have entered the wrong letter your number of tries decreases!\n\n");
    printf("When you are ready please press a button\n\n");
    system("pause");
    system("cls");
}

//draws the bulls and cows
void DrawBullsAndCows(){

#ifdef SHOW_HIDDEN_WORD
    printf("\nHidden Word: %s\n", HiddenWord);
    printf("Hidden Word length: %d\n",strlen(HiddenWord));

#endif // SHOW

    printf("\n");
    printf("         {   }  ()___()           \n");
    printf("         (o o)   (o o)          \n");
    printf("  /-------\\ /     \\ /------\\ \n");
    printf(" / | BULL |O       O| COW | \\  \n");
    printf("*  | -,---|         |-----|  *  \n");
    printf("   ^      ^         ^     ^\n   \n");
    printf("_________________________________\n");
}

//asks the player if it want to play again
bool AskToPlayAgain(){

    char *user = (char*)malloc(sizeof(char)*2);
    bool decide = false;

    printf("\n\nDo You Want to Play Again?\n\n");
    printf("Press 1 to Yes, 0 to No, then press enter\n\n");
    printf("I Chose: ");

    scanf("%s", user);

    printf("\n");

    if(*user == 49)
        decide = true;

    else
        decide = false;

    system("cls");
    return decide;
}
